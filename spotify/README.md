# SPOTIFY - POSTMAN
+ Generamos una petición en `Postman` a través de la API de `Spotify` de nuestro artista favorito.

## PASOS
+ Nosotros hemos seguido otro método para poder conseguir las 5 canciones más populares de nuestro artista `Justin Quiles`

![](capturas/spotify1_miguel.png)

1. Con nuestra cuenta normal de `Spotify` hemos accedido a la web de developer: `https://developer.spotify.com`

![](capturas/spotify2_miguel.png)

2. Hemos introducido nuestra cuenta y hemos creado nuestro `client ID` y nuestro `secret ID`

3. En opciones, hemos editado la parte de redirección para comunicar con postman en las peticiones:
```
Redirect URIs
https://app.getpostman.com/oauth2/callback
```

4. Accedemos a nuestro artista favorito para encontrar el ID del artista y observamos que Justin Quiles es `14zUHaJZo1mnYtn6IBRaRP`
```
https://open.spotify.com/artist/14zUHaJZo1mnYtn6IBRaRP
```
5. Vamos a la web `https://developer.spotify.com/console/get-artist-top-tracks` para conseguir la petición de las mejores canciones de nuestro artista una vez conseguido el token.
```
Indicamos
ID: 14zUHaJZo1mnYtn6IBRaRP
Country: ES
OAuth Token: seleccionamos -> playlist-modify-private playlist-read-private para conseguir el token
```

![](capturas/spotify3_miguel.png)

6. Esto nos dará nuestro `curl` para poder hacer la petición en Postman:
```
curl -X "GET" "https://api.spotify.com/v1/artists/14zUHaJZo1mnYtn6IBRaRP/top-tracks?country=ES" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer BQCyhGkGGxPkSLYE0a59XXQfoUnA3vL1eOZoEyTQaDXgzCGr6sEDpVVUnq_htP8ln-gtCiS96T_wGTV8CUsQL6aAl8wfSLYkGbI1VekDdiW7lkvVSAV0JZD4W7ZIq4GDgbMvmIdZOngimtK9am-sXKiQaKU0ZlQSH0t_hRP8z6U9rMokPfVZOC4"
```

![](capturas/spotify3_miguel.png)

7. Después vamos a `Postman` iniciamos sesión con una cuenta, creamos un `environment` que llamaremos __Spotify__ y crearemos también una colección con el mismo nombre para hacer las peticiones ahí dentro.

8. Por último pegaremos la `curl` que nos generó Spotify y recibiremos el resultado de la petición con las canciones más populares de `Justin Quiles`

![](capturas/spotify4_miguel.png)

![](capturas/spotify5_miguel.png)

![](capturas/spotify6_miguel.png)

9. El ejemplo de __Walid__ es con el artista `PNL` `https://open.spotify.com/artist/3NH8t45zOTqzlZgBvZRjvB` y su resultado es:

![](capturas/spotify7_walid.png)

![](capturas/spotify8_walid.png)


### EJEMPLO JSON DE UNA CANCIÓN - MIGUEL
```
{
      "album": {
        "album_type": "single",
        "artists": [
          {
            "external_urls": {
              "spotify": "https://open.spotify.com/artist/14zUHaJZo1mnYtn6IBRaRP"
            },
            "href": "https://api.spotify.com/v1/artists/14zUHaJZo1mnYtn6IBRaRP",
            "id": "14zUHaJZo1mnYtn6IBRaRP",
            "name": "Justin Quiles",
            "type": "artist",
            "uri": "spotify:artist:14zUHaJZo1mnYtn6IBRaRP"
          },
          {
            "external_urls": {
              "spotify": "https://open.spotify.com/artist/1GDbiv3spRmZ1XdM1jQbT7"
            },
            "href": "https://api.spotify.com/v1/artists/1GDbiv3spRmZ1XdM1jQbT7",
            "id": "1GDbiv3spRmZ1XdM1jQbT7",
            "name": "Natti Natasha",
            "type": "artist",
            "uri": "spotify:artist:1GDbiv3spRmZ1XdM1jQbT7"
          },
          {
            "external_urls": {
              "spotify": "https://open.spotify.com/artist/329e4yvIujISKGKz1BZZbO"
            },
            "href": "https://api.spotify.com/v1/artists/329e4yvIujISKGKz1BZZbO",
            "id": "329e4yvIujISKGKz1BZZbO",
            "name": "Farruko",
            "type": "artist",
            "uri": "spotify:artist:329e4yvIujISKGKz1BZZbO"
          }
        ],
        "external_urls": {
          "spotify": "https://open.spotify.com/album/71uU1JDWZ61OMDtW8h1Kp8"
        },
        "href": "https://api.spotify.com/v1/albums/71uU1JDWZ61OMDtW8h1Kp8",
        "id": "71uU1JDWZ61OMDtW8h1Kp8",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b27361b418f21b955326dcddd014",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e0261b418f21b955326dcddd014",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d0000485161b418f21b955326dcddd014",
            "width": 64
          }
        ],
        "name": "DJ No Pare (feat. Zion, Dalex, Lenny Tavárez) [Remix]",
        "release_date": "2019-09-06",
        "release_date_precision": "day",
        "total_tracks": 1,
        "type": "album",
        "uri": "spotify:album:71uU1JDWZ61OMDtW8h1Kp8"
      }
```

### EJEMPLO JSON DE UNA CANCIÓN - WALID
```
"album": {
        "album_type": "album",
        "artists": [
          {
            "external_urls": {
              "spotify": "https://open.spotify.com/artist/3NH8t45zOTqzlZgBvZRjvB"
            },
            "href": "https://api.spotify.com/v1/artists/3NH8t45zOTqzlZgBvZRjvB",
            "id": "3NH8t45zOTqzlZgBvZRjvB",
            "name": "PNL",
            "type": "artist",
            "uri": "spotify:artist:3NH8t45zOTqzlZgBvZRjvB"
          }
        ],
        "external_urls": {
          "spotify": "https://open.spotify.com/album/2JtKf1aFxqS0M3QIj98nG5"
        },
        "href": "https://api.spotify.com/v1/albums/2JtKf1aFxqS0M3QIj98nG5",
        "id": "2JtKf1aFxqS0M3QIj98nG5",
        "images": [
          {
            "height": 640,
            "url": "https://i.scdn.co/image/ab67616d0000b2736c3966c4dd0eb2273696fe16",
            "width": 640
          },
          {
            "height": 300,
            "url": "https://i.scdn.co/image/ab67616d00001e026c3966c4dd0eb2273696fe16",
            "width": 300
          },
          {
            "height": 64,
            "url": "https://i.scdn.co/image/ab67616d000048516c3966c4dd0eb2273696fe16",
            "width": 64
          }
        ],
        "name": "Deux frères",
        "release_date": "2019-04-05",
        "release_date_precision": "day",
        "total_tracks": 16,
        "type": "album",
        "uri": "spotify:album:2JtKf1aFxqS0M3QIj98nG5"
      }
```
