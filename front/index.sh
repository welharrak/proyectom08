#!/bin/bash

hour=$(date +"%H")

if [ $hour -ge 6 -a $hour -lt 14 ]; then
  firefox mati.html
elif [ $hour -ge 14 -a $hour -lt 22 ]; then
  firefox tarda.html
else
  firefox nit.html
fi
