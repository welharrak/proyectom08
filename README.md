# ProyectoM08 - Streaming  
+ Este proyecto trata de montar un servidor que contenga contenido multimedia, es decir, un servidor en streaming.  
+ Podremos acceder a él a través de la dirección IP o a través de la aplicación para móviles.  
+ También hemos creado una pequeña aplicación que nos permita configurar la reproducción de cierto video según a la hora a la que estemos.

## Estructura
+ Podremos ver diferentes carpetas:

### Install
+ Aquí tendremos las instalaciones y configuraciones (servidor, cliente y reproductor)

### Front
+ Aquí tendremos los ficheros correspondientes a nuestra aplicación:  
+ Tendremos un home con el programa y los html correspondientes, donde miraremos la hora del día:
    + Si es de 06:00 a 13:59, mostraremos el html 01(mañana).
    + Si es de 14:00 a 21:59, mostraremos el html 02(tarde).
    + Si es de 22:00 a 5:59, mostraremos el html 03(noche).
+ Código index.sh: Este programa lo hemos programado con bash, lo que hace básicamente es comprobar la hora del día, y dependiendo de la hora que sea, abre el firefox con el html correspondiente.

### Spotify
+ Aquí tendremos los pasos que hemos seguido para llevar a cabo la obtención de las mejores canciones de nuestro artista favorito utilizando **Spotify** y **Postman**.

