# SERVIDOR STREAMING
+ En nuestro proyecto hemos instalado dos servers: __PLEX__ para ser nuestro sitio donde tendremos almacenado nuestro contenido multimedia, y __KODI__, el cual se puede añadir un addon de PLEX para unir su contenido. Hemos tenido que hacer esta opción porque PLEX no nos generaba los links directos a un contenido en particular para poder hacer nuestra aplicación con __GIT__.

## PLEX MEDIA SERVER
### INSTALACIÓN
+ Dos formas:
  + Página web oficial [plex](https://www.plex.tv/media-server-downloads/):
     -  Bajamos la versión según sistema operativo y aquitectura.
     -  Instalamos:
      `rpm -i plexmediaserver-1.18.9.2571-e106a8a91.x86_64.rpm`
     -  Entramos en el navegador: 
      `http://localhost:32400/web/index.html`

  + Página externa(nuestra opción) [snaps](https://snapcraft.io/install/plexmediaserver/fedora)
     - Instalamos snap store:
      `dnf install snapd`
     - Creamos simbolic link de snap:
      `ln -s /var/lib/snapd/snap /snap`
     - Instalamos:
      `snap install plexmediaserver`
     -  Entramos en el navegador: 
      `http://localhost:32400/web/index.html`

> Los snaps son aplicaciones empaquetadas con todas sus dependencias para ejecutarse en todas las distribuciones populares de Linux desde una sola compilación. Se actualizan automáticamente.
> Los snaps se pueden instalar desde Snap Store, y lo hemos elegido por si hay otras aplicaciones que desde esta store nos puedan servir para el proyecto y complementar más fácilmente.

## KODI
### INSTALACIÓN
+ Instalamos:
`dnf install kodi -y`
+ Entramos en la terminal o en nuestra aplicaciones poniendo directamente: `kodi`


