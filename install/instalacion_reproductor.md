# Instalar un motor de reproducción Multimedia

## VLC

### KODI
- Descargamos VLC en nuestro dispositivo:  
[VLC](https://www.videolan.org/vlc/)

- En nuestro caso lo instalamos en nuestro ordenador con sistema operativo FEDORA 27: 
`dnf install vlc`

> Nota: por ahora funciona en dispositivo móvil y windows

- Tenemos que añadir una nueva fuente en Kodi para poder conectar reproductores externos.

- Vamos a ajustes -> explorador de archivos -> añadir fuente

- Añadimos la fuente: `https://kodiadictos.github.io/fuente/`

- Después volvemos al explorador de archivos y entramos en `directorio del perfil`.

- Una vez dentro vamos a la parte derecha y entramos en la fuente creada de `kodiadictos`

- Aquí vamos a `reproductor externo en kodi` -> `VLC` -> `playercorefactory.xml`

- Seleccionamos este fichero, le damos a copiar y automáticamente se pegará en la parte izquierda de `directorio del perfil`

- Una vez hecho esto, tendremos que salir de Kodi para que se guarden los cambios.

- Ya podremos seleccionar un video de nuestros archivos, mantenemos pulsado y nos saldrá la opción de `reproducir usando...` y seleccionaremos `VLC` y nuestro vídeo se verá con VLC.

![](capturas/vlckodi1.png)

![](capturas/vlckodi2.png)

![](capturas/vlckodi3.png)

![](capturas/vlckodi4.png)

### PLEX
- Descargamos VLC en nuestro dispositivo:  
[VLC](https://www.videolan.org/vlc/)

- En nuestro caso lo instalamos en nuestro ordenador con sistema operativo FEDORA 27: 
`dnf install vlc`

- Abrimos el VLC en el dispositivo remoto y seleccionamos `ver/view`

- Seleccionamos `Playlist` y desplácese hacia abajo en el panel izquierdo hasta llegar a `Local Network`.

- Seleccionamos `Universal Plug n Play` y espere a que se complete el cuadro de la derecha.

- Seleccionamos las carpetas en el panel derecho para encontrar el medio que desea reproducir de nuestro server Plex

- Hacemos doble clic en un medio específico y debería comenzar a reproducirse inmediatamente en VLC.

> Si todo va bien, los medios de comunicación tocarán en VLC sin problemas. Si el panel derecho no se rellena o no encuentra ningún medio, compruebe que DLNA está habilitado en Plex Media Server. Navegue hasta `Configuración y servidor en Plex Media Server` y asegúrese de que la casilla junto a `Servidor DLNA` está marcada. Debería ser por defecto. 
Active también Direct Play y Direct Stream si no lo están ya.

![](capturas/vlcplex1.png)

![](capturas/vlcplex2.png)

