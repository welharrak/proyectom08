# CONFIGURACIÓN CLIENTE
## PLEX MEDIA SERVER
### VÍA WEB
+ Web: `https://plex.tv/web`
+ Entramos con:
    - email: miguel14amoros@gmail.com
    - password: jupiter2020
+ A continuación el cliente entrará según si tiene perfil de:
    - Administrador (con PIN)
    - Usuario específico (con PIN)
    - Anónimo

### VÍA IP
+ Navegador: 
`Privado-> 192.168.1.41:32400`
`Público-> 90.168.170.12:21851 `
+ Entramos con:
    - email: miguel14amoros@gmail.com
    - password: jupiter2020
+ A continuación el cliente entrará según si tiene perfil de:
    - Administrador (con PIN)
    - Usuario específico (con PIN)
    - Anónimo

### VÍA APP
+ Bajar aplicación:
    - IOS: [Plex Apple Store](https://apps.apple.com/es/app/plex-movies-tv-music-more/id383457673)
    - ANDROID: [Plex Google Play](https://play.google.com/store/apps/details?id=com.plexapp.android&hl=es)
+ Entramos con:
    - email: miguel14amoros@gmail.com
    - password: jupiter2020
+ A continuación el cliente entrará según si tiene perfil de:
    - Administrador (con PIN)
    - Usuario específico (con PIN)
    - Anónimo

## KODI
### VÍA SOFTWARE
+ Tener instalado el software KODI con `dnf install -y kodi`
+ Entramos por terminal o aplicación poniendo `kodi`
+ Podremos navegar por las diferentes carpetas para visualizar el contenido que tengamos subido en Kodi o a través del Addon PLEX para ver el contenido en nuestro servidor de streaming PLEX.

### VÍA IP
+ Navegador: 
`Privado->  http://tu-dirección-ip: 8080` /  `http://localhost: 8080`
+ Entramos con:
    - usuario: miguelwalid
    - password: jupiter2020
+ Podremos navegar por las diferentes carpetas para visualizar el contenido que tengamos subido en Kodi o a través del Addon PLEX para ver el contenido en nuestro servidor de streaming PLEX.

### VÍA APP
+ Bajar aplicación:
    - IOS: No disponible en sitio oficial
    - ANDROID: [KODI Google Play]https://play.google.com/store/apps/details?id=org.xbmc.kodi&hl=es)
+ Podremos navegar por las diferentes carpetas para visualizar el contenido que tengamos subido en Kodi o a través del Addon PLEX para ver el contenido en nuestro servidor de streaming PLEX.

![](capturas/cliente1.png)

![](capturas/cliente2.png)

![](capturas/cliente3.png)

