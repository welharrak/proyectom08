# PLEX MEDIA SERVER
## CONFIGURACIÓN
+  Entramos en el navegador: 
 `http://localhost:32400/web/index.html`
+ Nos tendremos que registrar, en nuestro caso solo hemos creado una cuenta que usaremos los dos o para invitar a gente y entren con esta cuenta, previo creado un usuario para esa persona y ciertas restricciones.

![](https://i.blogs.es/c68be6/registrarte-en-plex/1366_2000.png)

+ Siguiente paso será ponerle un nombre a nuestro proyecto/servidor. Hemos elegido `MyNetflix`. Marcamos también la opción que se indica de poder acceder a los archivos desde fuera de casa.

![](https://i.blogs.es/cd2add/elige-nombre-para-el-servidor/1366_2000.png)

+ A continuación lo que haremos será añadir las bibliotecas desde donde sincronizarán los archivos que subamos. Lo que hemos hecho es crear una serie de directorio en nuestro home (películas, videos, fotos...) donde aquí meteremos los archivos que queramos subir a nuestro servidor.

![](https://i.blogs.es/a0207f/bibliotecas-de-plex/1366_2000.jpg)

![](https://i.blogs.es/460dba/crear-biblioteca-en-plex/1366_2000.png)

+ Finalmente, con este último paso, veremos el tablero de nuestro server donde podremos ver los ficheros que se han cargado una vez subidos a las bibliotecas creadas.
+ En nuestro perfil - usuarios, hemos creado tres:
  - Miguel&Walid (con pin) para rol de administrador.
  - Anna (con pin) rol para poder ver los contenidos únicamente.
  - Anonimo (sin pin) rol para poder ver los contenidos únicamente.

> Fuente: [xataka](https://www.xataka.com/basics/plex-que-es-y-como-funciona)

# KODI
## CONFIGURACIÓN
+ Entramos por teminal o aplicaciones poniendo: `kodi`
+ Para poder entrar a Kodi a través del navegador seguiremos estos pasos:
    - Kodi Configuración - Ajuste de servicio.
    - Eliges Control del menu lateral.
    - Activamos Permitir Control remoto a través de HTTP.
    - Permitir control remoto desde aplicaciones de otros equipos.
    - Permitir control remoto desde aplicaciones de este equipo.
    - Es opcional el nombre de usuario.
    - Es opcional también el uso de una contraseña.
    - Interfaz web eligen Kodi web interface - Chorus2
    - Tener en cuenta el número de puerto (por defecto es 8080).

+ Una ves hecha esta configuración podemos entrar:    
    - Para el acceso desde el mismo ordenador: `http://localhost:8080` en un navegador Web.
    - Para acceder desde otro ordenador: Obtener la dirección IP para el dispositivo que ejecuta Kodi. Por ejemplo, "ajustes- Información del sistema" y anote la dirección IP y abra http://tu-dirección-ip: 8080 en un navegador web.

# UNIR PLEX CON KODI
+ Entramos por ejemplo en la aplicación KODI y seguimos estos pasos:
+ Ir al menú `Addons` en el menú lateral izquierdo y, sin pulsar en Add-ons, seleccionamos `Entrar en el navegador de add-ons`

![](https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/styles/855/public/media/image/2018/07/como-instalar-plex-como-add-kodi-fusionar-dos_3.jpg?itok=JF_-Poql)

+ Veremos la lista de todos los complementos oficiales. Bajamos hasta la letra P, y pulsamos en el add-on de Plex:

![](https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/styles/855/public/media/image/2018/07/como-instalar-plex-como-add-kodi-fusionar-dos_4.jpg?itok=XcDv1mI_)

+ Solo queda pinchar en Instalar, para completar el proceso. Tened en cuenta que al instalar Plex se instalarán algunos add-ons de apoyo adicionales, necesarios para que Plex funcione como un add-on en Kodi.

![](https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/styles/855/public/media/image/2018/07/como-instalar-plex-como-add-kodi-fusionar-dos_5.jpg?itok=Ekc2E5QR)

+ Entramos a PLEX y nos pedirá un código para linkar con nuestra cuenta del servidor. Tendremos que poner `https://www.plex.tv/link/`
y en KODI pondremos el código que nos sale y quedará vinculado.

![](https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/styles/855/public/media/image/2018/07/como-instalar-plex-como-add-kodi-fusionar-dos_6.jpg?itok=sCLezpmf)

![](https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/styles/855/public/media/image/2018/07/como-instalar-plex-como-add-kodi-fusionar-dos_7.jpg?itok=HEgN4aWm)

> Fuentes:
 [hobbyconsolas](https://www.hobbyconsolas.com/reportajes/como-instalar-plex-como-add-kodi-fusionar-dos-282697)
> [pachinn](https://www.pachinn.com/2017/03/chorus2-control-de-kodi-desde-tu-navegador-web.html)
